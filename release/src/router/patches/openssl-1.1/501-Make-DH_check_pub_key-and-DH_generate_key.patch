From 46a4a9079c16a8ec6e0bb7b437ad9894a5863961 Mon Sep 17 00:00:00 2001
From: David Fernandez Gonzalez <david.fernandezgonzalez@canonical.com>
Date: Fri, 16 Feb 2024 16:41:31 +0100
Subject: [PATCH] Make DH_check_pub_key() and DH_generate_key() safer yet

Gbp-Pq: CVE-2023-5678.patch.
---
 crypto/dh/dh_check.c    | 14 ++++++++++++++
 crypto/dh/dh_err.c      |  1 +
 crypto/dh/dh_key.c      | 12 ++++++++++++
 crypto/err/openssl.txt  |  1 +
 include/openssl/dh.h    |  6 ++++--
 include/openssl/dherr.h |  1 +
 6 files changed, 33 insertions(+), 2 deletions(-)

diff --git a/crypto/dh/dh_check.c b/crypto/dh/dh_check.c
index 285434121..df2c4399f 100644
--- openssl-1.1/crypto/dh/dh_check.c
+++ openssl-1.1/crypto/dh/dh_check.c
@@ -202,6 +202,19 @@ int DH_check_pub_key(const DH *dh, const BIGNUM *pub_key, int *ret)
     if (ctx == NULL)
         goto err;
     BN_CTX_start(ctx);
+
+    /* Don't do any checks at all with an excessively large modulus */
+    if (BN_num_bits(dh->p) > OPENSSL_DH_CHECK_MAX_MODULUS_BITS) {
+        DHerr(DH_F_DH_CHECK, DH_R_MODULUS_TOO_LARGE);
+        *ret = DH_MODULUS_TOO_LARGE | DH_CHECK_PUBKEY_INVALID;
+        goto err;
+    }
+
+    if (dh->q != NULL && BN_ucmp(dh->p, dh->q) < 0) {
+        *ret |= DH_CHECK_INVALID_Q_VALUE | DH_CHECK_PUBKEY_INVALID;
+        goto out;
+    }
+
     tmp = BN_CTX_get(ctx);
     if (tmp == NULL || !BN_set_word(tmp, 1))
         goto err;
@@ -220,6 +233,7 @@ int DH_check_pub_key(const DH *dh, const BIGNUM *pub_key, int *ret)
             *ret |= DH_CHECK_PUBKEY_INVALID;
     }
 
+ out:
     ok = 1;
  err:
     BN_CTX_end(ctx);
diff --git a/crypto/dh/dh_err.c b/crypto/dh/dh_err.c
index 8dd8ca0f9..98ac88edb 100644
--- openssl-1.1/crypto/dh/dh_err.c
+++ openssl-1.1/crypto/dh/dh_err.c
@@ -82,6 +82,7 @@ static const ERR_STRING_DATA DH_str_reasons[] = {
     {ERR_PACK(ERR_LIB_DH, 0, DH_R_PARAMETER_ENCODING_ERROR),
     "parameter encoding error"},
     {ERR_PACK(ERR_LIB_DH, 0, DH_R_PEER_KEY_ERROR), "peer key error"},
+    {ERR_PACK(ERR_LIB_DH, 0, DH_R_Q_TOO_LARGE), "q too large"},
     {ERR_PACK(ERR_LIB_DH, 0, DH_R_SHARED_INFO_ERROR), "shared info error"},
     {ERR_PACK(ERR_LIB_DH, 0, DH_R_UNABLE_TO_CHECK_GENERATOR),
     "unable to check generator"},
diff --git a/crypto/dh/dh_key.c b/crypto/dh/dh_key.c
index daffdf74d..1b1844686 100644
--- openssl-1.1/crypto/dh/dh_key.c
+++ openssl-1.1/crypto/dh/dh_key.c
@@ -114,6 +114,12 @@ static int generate_key(DH *dh)
         return 0;
     }
 
+    if (dh->q != NULL
+        && BN_num_bits(dh->q) > OPENSSL_DH_MAX_MODULUS_BITS) {
+        DHerr(DH_F_GENERATE_KEY, DH_R_Q_TOO_LARGE);
+        return 0;
+    }
+
     ctx = BN_CTX_new();
     if (ctx == NULL)
         goto err;
@@ -207,6 +213,12 @@ static int compute_key(unsigned char *key, const BIGNUM *pub_key, DH *dh)
         goto err;
     }
 
+    if (dh->q != NULL
+        && BN_num_bits(dh->q) > OPENSSL_DH_MAX_MODULUS_BITS) {
+        DHerr(DH_F_COMPUTE_KEY, DH_R_Q_TOO_LARGE);
+        goto err;
+    }
+
     ctx = BN_CTX_new();
     if (ctx == NULL)
         goto err;
diff --git a/crypto/err/openssl.txt b/crypto/err/openssl.txt
index 371524d5c..c59502b8c 100644
--- openssl-1.1/crypto/err/openssl.txt
+++ openssl-1.1/crypto/err/openssl.txt
@@ -2106,6 +2106,7 @@ DH_R_NO_PARAMETERS_SET:107:no parameters set
 DH_R_NO_PRIVATE_VALUE:100:no private value
 DH_R_PARAMETER_ENCODING_ERROR:105:parameter encoding error
 DH_R_PEER_KEY_ERROR:111:peer key error
+DH_R_Q_TOO_LARGE:130:q too large
 DH_R_SHARED_INFO_ERROR:113:shared info error
 DH_R_UNABLE_TO_CHECK_GENERATOR:121:unable to check generator
 DSA_R_BAD_Q_VALUE:102:bad q value
diff --git a/include/openssl/dh.h b/include/openssl/dh.h
index 892e31559..a70c51e87 100644
--- openssl-1.1/include/openssl/dh.h
+++ openssl-1.1/include/openssl/dh.h
@@ -71,14 +71,16 @@ DECLARE_ASN1_ITEM(DHparams)
 /* #define DH_GENERATOR_3       3 */
 # define DH_GENERATOR_5          5
 
-/* DH_check error codes */
+/* DH_check error codes, some of them shared with DH_check_pub_key */
 # define DH_CHECK_P_NOT_PRIME            0x01
 # define DH_CHECK_P_NOT_SAFE_PRIME       0x02
 # define DH_UNABLE_TO_CHECK_GENERATOR    0x04
 # define DH_NOT_SUITABLE_GENERATOR       0x08
 # define DH_CHECK_Q_NOT_PRIME            0x10
-# define DH_CHECK_INVALID_Q_VALUE        0x20
+# define DH_CHECK_INVALID_Q_VALUE        0x20 /* +DH_check_pub_key */
 # define DH_CHECK_INVALID_J_VALUE        0x40
+# define DH_MODULUS_TOO_SMALL            0x80
+# define DH_MODULUS_TOO_LARGE            0x100 /* +DH_check_pub_key */
 
 /* DH_check_pub_key error codes */
 # define DH_CHECK_PUBKEY_TOO_SMALL       0x01
diff --git a/include/openssl/dherr.h b/include/openssl/dherr.h
index 9955f2465..17397b7ba 100644
--- openssl-1.1/include/openssl/dherr.h
+++ openssl-1.1/include/openssl/dherr.h
@@ -82,6 +82,7 @@ int ERR_load_DH_strings(void);
 #  define DH_R_NO_PRIVATE_VALUE                            100
 #  define DH_R_PARAMETER_ENCODING_ERROR                    105
 #  define DH_R_PEER_KEY_ERROR                              111
+#  define DH_R_Q_TOO_LARGE                                 130
 #  define DH_R_SHARED_INFO_ERROR                           113
 #  define DH_R_UNABLE_TO_CHECK_GENERATOR                   121
 
-- 
cgit v1.2.3

